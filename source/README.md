# Source

- [jupyter](/jupyter): Go here to see data cleaning, clustering and downloading further data from Octopart API.
- [scrapy](/scrapy): Go here to find crawlers for company websites that search and download publicly available FMDs (Full Material Descriptions).