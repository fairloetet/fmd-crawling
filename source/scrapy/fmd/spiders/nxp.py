from fmd.spiders import FMD_Base_Spider

# Downloads material descriptions as xml from www.nxp.com
# 1. crawls all product links (parse), 
# 2. downloads xmls from their /webapp/chemical_content (parse_product) and 
# 3. saves the material declaration (save_material_declaration)

class NXPSpider(FMD_Base_Spider):
    name = "nxp"
    start_urls = ['https://www.nxp.com']

    def __init__(self):
        super().__init__(self.name)
    
    def parse(self, response):
        # only follow webapp links or package-quality-tabs and no other links on the same page, if these are present
        # TODO: check whether that still downloads everything
        product_site_links = response.css('a::attr(href)').re(r'/webapp/chemical-content.*')
        package_quality_tabs = response.css('a::attr(href)').re(r'.*tab=Package_Quality_Tab')
        general_product_links = response.css('a::attr(href)').re(r'/products/.*')

        if product_site_links:
            for link in product_site_links:
                yield response.follow(link, callback = self.parse_product)
        else:
            if package_quality_tabs:
                for link in package_quality_tabs:
                    yield response.follow(link, callback = self.parse)
            else:
                for link in general_product_links:
                    yield response.follow(link, callback = self.parse)

    def parse_product(self, response):
        product_name = response.css('h1::text').get()
        for download_link in response.css('table.table td a::attr(href)').re(r'.*ipc1752A/xml'):
            download_link = download_link.split("/")[-2] + "/" + download_link.split("/")[-1]
            download_url = 'https://www.nxp.com/webapp/chemical-content/' + download_link + "/" + product_name + ".zip"
            yield response.follow(download_url, callback = self.save_xml_zip_as_xml)

