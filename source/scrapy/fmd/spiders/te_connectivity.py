import re
from fmd.spiders import FMD_Base_Spider

"""You may download Content only for your personal use for non-commercial purposes, 
provided you also retain all copyright and other proprietary notices, 
but no modification or further reproduction of the Content is permitted."""


class TE_Connectivity_Spider(FMD_Base_Spider):
    name = "te_connectivity"
    start_urls = ['https://www.te.com/usa-en/home.html']

    def __init__(self):
        super().__init__(self.name)

    def parse(self, response):
        menu_links = response.css("div.menu a::attr(href)").re(r'.*products.*')
        view_all_links = response.css("div.view-all a::attr(href)").re(r'.*/plp/.*')
        product_category_links = response.css('div.product-category a::attr(href)').re(r'.*/products/.*')
        plp_links = response.css("div.product-category a::attr(href)").re(r'.*/plp/.*')
        product_ids = response.xpath('//span[@itemprop="productID"]/text()').getall()

        if len(product_ids) > 0:
            for product_id in product_ids:
                link = "/usa-en/product-" + product_id + ".html"
                yield response.follow(link, callback = self.parse_product)

            page_url_parts = response.url.split('?p=')
            if len(page_url_parts) > 1:
                next_page_url = page_url_parts[0] + "?p=" + str(int(page_url_parts[1]) + 1)
            else :
                next_page_url = page_url_parts[0] + "?p=2"
            yield response.follow(next_page_url, callback=self.parse)

        else:
            if len(view_all_links) > 0:
                for link in view_all_links:
                    yield response.follow(link, callback = self.parse)
            elif len(product_category_links) > 0 or len(plp_links) > 0:
                for link in product_category_links:
                    yield response.follow(link, callback = self.parse)

                for link in plp_links:
                    yield response.follow(link, callback = self.parse)
            else:
                for link in menu_links:
                    yield response.follow(link, callback = self.parse)

    def parse_product(self, response):
        # if there is Product Environmental Compliance, download pdfs or xmls from there
        # te-connectivity has two different html structures for serving compliance links
        # 1. selects all links where data-doc-title = "Product Compliance"
        product_compliance_links = response.xpath('//a[@data-doc-title="Product Compliance"]/@href').getall()
        product_compliance_filenames = response.xpath('//a[@data-doc-title="Product Compliance"]/text()').getall()
        
        if len(product_compliance_links) == 0:
            # 2. if 1. did not return results, selects all links where data-doc-title = MD_...._dmtec
            product_compliance_links = response.xpath('//a[@data-doc-title[re:test(., "MD.*dmtec")]]/@href').getall()
            product_compliance_filenames = response.xpath('//a[@data-doc-title[re:test(., "MD.*dmtec")]]/text()').getall()

        if len(product_compliance_links) == 0:
            # we still do not have any product compliance links, so there probably are none on the page
            return

        # filter if there is an xml, otherwise pdf, otherwise xlsx, otherwise just download
        xml_regex = re.compile(".*\.xml")
        pdf_regex = re.compile(".*\.pdf")
        xlsx_regex = re.compile(".*\.xlsx")
        xml_list = list(filter(xml_regex.match, product_compliance_links))
        pdf_list = list(filter(pdf_regex.match, product_compliance_links))
        xlsx_list = list(filter(xlsx_regex.match, product_compliance_links))

        prioritized_lists = [xml_list, pdf_list, xlsx_list, product_compliance_links]

        for regex_list in prioritized_lists:
            if len(regex_list) > 0:
                for link in regex_list:
                    i = product_compliance_links.index(link)
                    filename = product_compliance_filenames[i]
                    yield response.follow(link, callback = self.save_material_declaration, cb_kwargs = {"filename": filename.strip()})
                break

    def save_material_declaration(self, response, filename):
        file_ending = filename.split('.')[-1]
        if "xml" == file_ending:
            self.save_xml(response, filename)
        elif "pdf" == file_ending:
            self.save_pdf(response, filename)
        else:
            self.save_bytes(response, filename)
            # self.logger.warning('Could not download file: %s' % filename)

