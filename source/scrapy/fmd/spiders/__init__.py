# This package will contain the spiders of your Scrapy project

# The following is an abstract base spider for the FMD crawling project. It should not be instantiated on its own, 
# it only provides shared functionality for all the other spiders
import scrapy
from zipfile import ZipFile
from io import BytesIO

class FMD_Base_Spider(scrapy.Spider):
    def __init__(self, name = "fmd"):
        self.data_dir = "data"
        self.data_dir_path = f'{self.data_dir}/{self.name}'
        self.name = name
        self.logfile_name = "log.txt"
        self.logfile_path = f'{self.data_dir_path}/{self.logfile_name}'

    def log_download(self, response, filename):
        with open(self.logfile_path, "a") as logfile:
            logfile.write(f'{filename} {response.url}\n')


    def save_xml_zip_as_xml(self, response):
        with ZipFile(BytesIO(response.body)) as zfile:
            zfile.extractall(self.data_dir_path)
            if len(zfile.namelist()) > 0:
                for member in zfile.namelist():
                    self.log_download(response, member)
            else:
                self.log_download(response, zfile.namelist())

    def save_bytes(self, response, filename):
        self.log_download(response, filename)
        filepath = f'{self.data_dir_path}/{filename}'
        with open(filepath, "wb") as file:
            file.write(response.body)
            file.close()

    def save_xml(self, response, filename):
        self.save_bytes(response, filename)

    def save_pdf(self, response, filename):
        self.save_bytes(response, filename)
