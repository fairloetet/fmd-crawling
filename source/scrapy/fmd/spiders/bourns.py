from fmd.spiders import FMD_Base_Spider
import re

class BournsSpider(FMD_Base_Spider):
    name = 'bourns'
    start_urls = ["https://bourns.com/products"]

    def __init__(self):
        super().__init__(self.name)

    def parse(self, response):

        product_links = response.css('a::attr(href)').re(r'/products/.*')
        fmd_links = response.css('a::attr(href)').re(r'/docs/Product-MDS/.*')

        if product_links:
            for link in product_links:
                yield response.follow(link, callback = self.parse)

        if fmd_links:
            for link in fmd_links:
                #print(link)
                filename = re.match(r'.*/Product-MDS/(.+\.pdf).*', link).group(1)
                #print(filename)
                yield response.follow(link, callback = self.save_pdf, cb_kwargs = {"filename": filename})