# FMD Crawling

To run the NXP crawler which downloads hopefully all material declaration files as XML files from <https://www.nxp.com>, please follow these steps:
1. connect to server (v2202107106506157970.quicksrv.de)
2. cd into scrapy directory (/source/scrapy/)
2. start a tmux session with `tmux new`
2. open a poetry shell
3. run `$ scrapy crawl nxp` or `$ scrapy crawl te_connectivity` from this directory
4. you should now have started a spider that crawls the NXP (TE) Website for quite some time. Meanwhile there should now be a data directory where the downloaded files are saved and keep piling up :)
5. if you want to keep it running, type Ctrl-B, then press d to detach from the tmux session. To reattach, type `tmux attach`

Coming Soon:
- more files from other companies
- more spiders ;)

## Summary of NXP Crawling

- Started: 22.07.21, 10:33:32 (start time in log + 2 hours)
- Finished: 24.07.21, 20:03:30
- Took: 57,5 h
- request rate: around every 3 seconds
- 16,4 pages per min
- 56608 pages
- Response codes:
    - 200: 55647 (ok)
    - 301: 89 (redirect)
    - 302: 3 (redirect)
    - 404: 130 (Not Found)
    - 503: 739 (Service Unavailable)
- 22066 xml files downloaded (11033 items)
- later deleted the IPC1752-2.xml files

Was not the fastest crawler, could be faster with fewer request delay and more direct crawling (which is now implemented)

## Second Run of NXP Crawling
Data from the first run can be found in data_save/nxp.

- Started: 28.07.21, 14:44:40
- Finished: 29.07.21, 11:34:01
- Took: 20,8 h
- request rate: around every 2 seconds
- 24,6 pages per min
- 30749 pages
- Response codes:
    - 200: 30651 (ok)
    - 301: 77 (redirect)
    - 302: 3 (redirect)
    - 404: 18 (Not Found)
    - 503: 0 (Service Unavailable)
- 10968 xml files downloaded
- missing 65 files compared to first run


## Crawling of TE Connectivity
as this website grew over time, it is quite inconsistent, so I have no guarantee that my crawler fetches all of their documents, but enough for now

- Started: 13.08.2021, 10:04:25
- Finished: 20.08.2021, 02:06:15
- Took: 160 h
- request rate: around every 2 seconds
- 24,6 pages per min
- 236161 pages
- Response codes:
    - 200: 236137 (ok)
    - 404: 3 (Not Found)
    - 500: 21 (Internal Server Error)
- 16597 files downloaded
    - 8950 XML files
    - 7647 PDF files

    
## Bourns Crawling
Sadly, Bourns only provides PDFs, but it is another important source for parts. The website is very straightforward and therefor easy to crawl.

- Started: 04.05.2022, 10:15:12
- Finished: 04.05.2022, 16:48:17
- Took: 4,5 h
- request rate: around every 2 seconds
- 24,7 pages per min
- 6752 pages
- Response codes:
	- 200: 6204 (ok)
	- 301: 515 (redirect)
	- 302: 33 (redirect)
- 510 PDFs downloaded