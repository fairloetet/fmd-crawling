import json, pandas, re
from typing import List, Dict, Tuple
from scripts.constants import SUBSTANCES_FOLDER, SUBSTANCES_JSON_FOLDER, UNKNOWN_SUBSTANCE_IDENTIFIER, UNKNOWN_SUBSTANCE_NAME, DELIMITER
from decimal import Decimal, getcontext

from scripts.constants import LIST_DELIMITER

def create_dict_cas_to_name(substances_file_name: str, number_to_cas: Dict[str, str], output_file_name: str):
    substances = pandas.read_csv(SUBSTANCES_FOLDER + substances_file_name, index_col = 0)
    cas_number_to_substance_name = {}
    cas_number_to_substance_name[UNKNOWN_SUBSTANCE_IDENTIFIER] = UNKNOWN_SUBSTANCE_NAME
    for (name, authority, identity, counts) in substances.itertuples(index=False):
        correct_identity = number_to_cas[identity]
        if not correct_identity in cas_number_to_substance_name:
            cas_number_to_substance_name[correct_identity] = name

    save_json(SUBSTANCES_JSON_FOLDER + output_file_name, cas_number_to_substance_name)

def create_dict_name_to_name(substances_file_names: List[str], cas_to_name: Dict[str, str], output_file_name: str):
    name_to_substance_name = {}
    name_to_substance_name[UNKNOWN_SUBSTANCE_NAME] = UNKNOWN_SUBSTANCE_NAME
    for file_name in substances_file_names:
        substances = pandas.read_csv(SUBSTANCES_FOLDER + file_name, index_col = 0)
        for (name, authority, identity, counts) in substances.itertuples(index=False):
            if isNan(identity):
                identity = UNKNOWN_SUBSTANCE_IDENTIFIER
            if not name in name_to_substance_name:
                if identity in cas_to_name:
                    name_to_substance_name[name] = cas_to_name[identity]
                else:
                    name_to_substance_name[name] = UNKNOWN_SUBSTANCE_NAME

    save_json(SUBSTANCES_JSON_FOLDER + output_file_name, name_to_substance_name)

def create_dict_number_to_cas_number(substances_file_names: List[str], output_file_name: str):
    number_to_cas_number = {}
    number_to_cas_number[UNKNOWN_SUBSTANCE_IDENTIFIER] = UNKNOWN_SUBSTANCE_IDENTIFIER
    for file_name in substances_file_names:
        substances = pandas.read_csv(SUBSTANCES_FOLDER + file_name, delimiter = DELIMITER)
        for (name, authority, identity) in substances.itertuples(index=False):
            if not isNan(identity) and re.match('^[0-9]{2,7}-[0-9]{2}-[0-9]$', str(identity)):
                identity = str(identity)
                number_to_cas_number[identity] = identity
            else:
                number_to_cas_number[str(identity)] = str(UNKNOWN_SUBSTANCE_IDENTIFIER)
    save_json(SUBSTANCES_JSON_FOLDER + output_file_name, number_to_cas_number)

def save_json(filename: str, dict_to_save: Dict[str, str]):
    with open(filename, 'w', encoding='utf-8') as dictfile:
        json.dump(dict_to_save, dictfile, sort_keys=True, indent=4, ensure_ascii=False)

def replace_substance_strings(strings: List[List[str]], map: List[Dict[str, str]], unknown_strings : List[str]) -> List[List[str]]:
    results = []
    for index, string_list in enumerate(strings):
        new_substance_strings = []
        for substance_string in string_list:
            if substance_string in map[index]:
                new_substance_strings.append(map[index][substance_string])
            else:
                new_substance_strings.append(unknown_strings[index])
        results.append(new_substance_strings)
    return results

def calculate_ratios(amounts: List[float]) -> List[float]:
    getcontext().prec = 15
    amounts = [Decimal(amount) for amount in amounts]
    total_amount = sum(amounts)
    if total_amount == 0:
        # just return empty list when ratios cannot be calculated
        return []
    else:
        ratios = []
        for amount in amounts:
            ratios.append(amount / total_amount)

    return [float(ratio) for ratio in ratios]

def create_ratio_lists(identifier_list: str, amounts_list: str, identifier_to_index: Dict[str, int]):
    identifier_list = convert_list_string_to_list(identifier_list)
    amounts_list = convert_list_string_to_float_list(amounts_list)
    ratio_list = calculate_ratios(amounts_list)
    data_list = [0] * len(identifier_to_index)
    if not ratio_list:
        return data_list, ""
    for i in range(len(identifier_list)):
        identifier = identifier_list[i]
        ratio = ratio_list[i]
        data_list[identifier_to_index[identifier]] += ratio
    ratio_list_string = LIST_DELIMITER.join([str(round(ratio, 4)) for ratio in ratio_list])
    return data_list, ratio_list_string

def convert_list_string_to_list(string_list: str) -> List[str]:
    if isNan(string_list):
        return []
    return string_list.split(LIST_DELIMITER)

def convert_list_string_to_int_list(string_list: str) -> List[int]:
    list = convert_list_string_to_list(string_list)
    return [int(el) for el in list]

def convert_list_string_to_float_list(string_list: str) -> List[float]:
    list = convert_list_string_to_list(string_list)
    return [float(el) for el in list]

def isNan(x):
    return x != x