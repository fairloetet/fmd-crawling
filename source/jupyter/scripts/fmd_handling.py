from lxml import objectify
import os
from typing import List, Tuple, Dict
from scripts.constants import MATERIALS_CSV_HEADER, SUBSTANCE_CSV_HEADER, MATERIAL_COMPOSITION_CSV_HEADER, COMPONENT_COMPOSITION_CSV_HEADER
from scripts.constants import DELIMITER, LIST_DELIMITER
from scripts.constants import MANUFACTURERS

def load_all_fmds(data_directories: List[str]) -> List[object]:
    fmds = []
    for directory in data_directories:
        for filename in os.listdir(directory):
            if filename.endswith(".xml"):
                with open(directory + filename, encoding="utf8") as xml_file:
                    xml = xml_file.read().encode()
                    main_declaration = objectify.fromstring(xml)
                    fmds.append(main_declaration)
    return fmds

def extract_substance_information(product: objectify.Element) -> List[List[str]]:
    results = []
    if hasattr(product, 'SubProduct'):
        for sub_product in product.SubProduct:
            results.extend(extract_substance_information(sub_product))
    elif hasattr(product, 'MaterialInfo') and hasattr(product.MaterialInfo, 'HomogeneousMaterialList'): 
        for homogeneous_material in product.MaterialInfo.HomogeneousMaterialList.HomogeneousMaterial: 
            for substance_category_list in homogeneous_material.SubstanceCategoryList:
                for substance in substance_category_list.SubstanceCategory.Substance:
                    substance_dict = {}
                    substance_dict["name"] = substance.get('name')
                    substance_dict["authority"] = substance.SubstanceID.get('authority')
                    substance_dict["identity"] = substance.SubstanceID.get('identity')
                    result = [substance_dict[key] for key in SUBSTANCE_CSV_HEADER]
                    results.append(result)
    return results

def extract_material_information(product: objectify.Element) -> List[List[str]]:
    results = []
    if hasattr(product, 'SubProduct'):
        for sub_product in product.SubProduct:
            results.extend(extract_material_information(sub_product))
    elif hasattr(product, 'MaterialInfo') and hasattr(product.MaterialInfo, 'HomogeneousMaterialList'):
        for homogeneous_material in product.MaterialInfo.HomogeneousMaterialList.HomogeneousMaterial:
            material_dict = {}
            material_dict["name"] = homogeneous_material.get('name')
            material_dict["material_group_name"] = homogeneous_material.get('materialGroupName')
            result = [material_dict[key] for key in MATERIALS_CSV_HEADER]
            results.append(result)
    return results

def extract_material_composition(product: objectify.Element) -> List[List[str]]:
    results = []
    if hasattr(product, 'SubProduct'):
        for sub_product in product.SubProduct:
            results.extend(extract_material_composition(sub_product))
    elif hasattr(product, 'MaterialInfo') and hasattr(product.MaterialInfo, 'HomogeneousMaterialList'):
        for homogeneous_material in product.MaterialInfo.HomogeneousMaterialList.HomogeneousMaterial:
            material_dict = {}
            material_dict["material_name"] = homogeneous_material.get('name').title()
            material_dict['substance_list'] = []
            material_dict['identifier_list'] = []
            material_dict['amounts_list'] = []
            for substance_category_list in homogeneous_material.SubstanceCategoryList:
                for substance in substance_category_list.SubstanceCategory.Substance:
                    material_dict['substance_list'].append(substance.get('name'))
                    material_dict['identifier_list'].append(substance.SubstanceID.get('identity'))
                    material_dict['amounts_list'].append(substance.Amount.get('value'))
            
            for string_list in ['substance_list', 'identifier_list', 'amounts_list']:
                material_dict[string_list] = LIST_DELIMITER.join(material_dict[string_list])
            result = [material_dict[key] for key in MATERIAL_COMPOSITION_CSV_HEADER]
            results.append(result)

    return results

def extract_component_composition(fmd: objectify.Element) -> List[List[str]]:
    result = []
    product = fmd.Product
    name = product.ProductID.get('itemName')
    if not name:
        name = 'No component name'
    manufacturer = fmd.BusinessInfo.Response.SupplyCompany.get('name')
    if not manufacturer:
        manufacturer = 'No manufacturer name'
    part_no = get_part_no(product, manufacturer)
    
    component_dict = {}
    for key in COMPONENT_COMPOSITION_CSV_HEADER:
        component_dict[key] = []
    go_through_subproducts(product, component_dict)

    for key in COMPONENT_COMPOSITION_CSV_HEADER:
        component_dict[key] = LIST_DELIMITER.join([str(item) for item in component_dict[key]])
    component_dict["name"] = name
    component_dict["part_no"] = part_no
    component_dict["manufacturer"] = manufacturer
    result = [component_dict[key] for key in COMPONENT_COMPOSITION_CSV_HEADER]
    return result

def get_part_no(product: objectify.Element, manufacturer: str) -> str:
    if manufacturer in MANUFACTURERS:
        part_no = product.ProductID.get(MANUFACTURERS[manufacturer]["part_id"])
        return part_no
    else:
        raise ValueError('Manufacturer name ' + manufacturer + ' is missing, consider adding it to constants.py!')


def go_through_subproducts(product: objectify.Element, component_dict: Dict[str, List[str]], number_of_instances : int = 1):
    if hasattr(product, 'SubProduct'):
        for sub_product in product.SubProduct:
            number_of_instances = number_of_instances * float(sub_product.get('numberOfInstances'))
            go_through_subproducts(sub_product, component_dict, number_of_instances)
    else:
        go_through_materials(product, component_dict, number_of_instances)

def go_through_materials(material_info: objectify.Element, component_dict : Dict[str, List[str]], number_of_instances : int):
    if hasattr(material_info, 'MaterialInfo') and hasattr(material_info.MaterialInfo, 'HomogeneousMaterialList'):
        for homogeneous_material in material_info.MaterialInfo.HomogeneousMaterialList.HomogeneousMaterial:
            name = homogeneous_material.get('name').title()
            component_dict['materials'].append(name)
            material_weight = float(homogeneous_material.Amount.get('value'))
            component_dict['material_amounts'].append(material_weight * number_of_instances)
            substance_count = 0
            for substance_category_list in homogeneous_material.SubstanceCategoryList:
                for substance in substance_category_list.SubstanceCategory.Substance:
                    component_dict['substances'].append(substance.get('name'))
                    component_dict['substance_identifiers'].append(substance.SubstanceID.get('identity'))
                    substance_weight = float(substance.Amount.get('value'))
                    component_dict['substance_amounts'].append(substance_weight * number_of_instances)
                    substance_count += 1
            component_dict['substance_counts'].append(substance_count)
        
    

def get_substances(data: List[List[object]]) -> List[Tuple[str]]:
    substances = []
    for fmd_list in data:
        for fmd in fmd_list:
            substances.extend(extract_substance_information(fmd.Product))
    return substances

def get_materials(data: List[object]) -> List[Tuple[str]]:
    materials = []
    for fmd in data:
        materials.extend(extract_material_information(fmd.Product))
    return materials

def get_material_compositions(data: List[object]) -> List[Tuple[str]]:
    materials = []
    for fmd in data:
        materials.extend(extract_material_composition(fmd.Product))
    return materials

def get_component_compositions(data: List[object]) -> List[Tuple[str]]:
    components = []
    for fmd in data:
        components.append(extract_component_composition(fmd))
    return components

def write_list_to_csv(filepath: str, data: List[Tuple[str]], header: List[str]):
    string_header = DELIMITER.join(header) + '\n'
    file = open(filepath, 'w')
    file.write(string_header)
    for row in data:
        if len(row) != len(header):
            print(row)
            print(header)
            raise ValueError('Size of header (' + str(len(header)) + ') does not match data rows (' + str(len(row)) + ')')
        else:
            file.write(DELIMITER.join(row) + '\n')
    file.close()
    
def create_substances_csvs(filepaths: List[str], data: List[List[object]]):
    for index, data_list in enumerate(data):
        substances = get_substances(data_list)
        write_list_to_csv(filepaths[index], substances, SUBSTANCE_CSV_HEADER)

def create_materials_csvs(filepaths: List[str], data: List[List[object]]):
    for index, data_list in enumerate(data):
        materials = get_materials(data_list)
        write_list_to_csv(filepaths[index], materials, MATERIALS_CSV_HEADER)

def create_material_composition_csvs(filepaths: List[str], data: List[List[object]]):
    for index, data_list in enumerate(data):
        material_compositions = get_material_compositions(data_list)
        write_list_to_csv(filepaths[index], material_compositions, MATERIAL_COMPOSITION_CSV_HEADER)

def create_components_csvs(filepaths: List[str], data: List[List[object]]):
    for index, data_list in enumerate(data):
        component_compositions = get_component_compositions(data_list)
        write_list_to_csv(filepaths[index], component_compositions, COMPONENT_COMPOSITION_CSV_HEADER)