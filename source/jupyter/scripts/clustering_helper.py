from typing import List, Tuple
from scripts.constants import LIST_DELIMITER
from sklearn.cluster import KMeans
from sklearn import metrics
from pandas import DataFrame
from matplotlib.pyplot import figure, plot, xlabel, ylabel, title, show
from decimal import Decimal

from scripts.constants import UNKNOWN_SUBSTANCE_IDENTIFIER

def expand_composite_ratio_lists (composite_names : List[List[str]], ratios : List[List[str]]) -> List[List[Tuple[str, str]]]:
    if len(composite_names) != len(ratios):
        raise ValueError('Lengths of argument lists do not match, got ' + str(len(composite_names)) + ' and ' + str(len(ratios)))
    result = []
    for i in range(0, len(composite_names)):
        if ratios[i] != ratios[i]:
            # current ratios are nan, thus empty
            result.append([])
            continue
        current_composite_names = composite_names[i].split(LIST_DELIMITER)
        current_ratios = ratios[i].split(LIST_DELIMITER)
        ratio_name_list = []

        if len(current_composite_names) != len(current_ratios):
            raise ValueError('Lengths of names and ratios do not match for entry ' + str(i) + ', got ' + str(current_composite_names) + ' and ' + str(current_ratios))
        for j in range(len(current_composite_names)):
            ratio_name_list.append((current_ratios[j], current_composite_names[j]))
        # sort all ratio-name tuples descending by ratio and add to result
        result.append(sorted(ratio_name_list, key = lambda x : x[0], reverse = True))

    return result

def clustering_with_metrics(data : DataFrame, cluster_amounts : List[int], elbow_method : bool = True, silhouette_coefficient : bool = True, silhouette_sample_ratio = 0.5) -> Tuple[List[float], List[float]]:
    elbow_metrics = []
    silhouette_metrics = []
    for k in cluster_amounts:
        kmeanModel = KMeans(n_clusters=k)
        kmeanModel.fit(data)
        if (elbow_method):
            elbow_metrics.append(kmeanModel.inertia_)
        if (silhouette_coefficient):
            silhouette_metrics.append(
                [metrics.silhouette_score(data, kmeanModel.labels_, metric="euclidean", sample_size=int(len(data)*silhouette_sample_ratio))]
            )
            

    if (elbow_method and silhouette_coefficient):
        return elbow_metrics, silhouette_metrics
    elif (elbow_method):
        return elbow_metrics
    elif (silhouette_coefficient):
        return silhouette_metrics

def draw_plot(cluster_amounts : List[int], metrics : List[float], name_of_metric : str):
    figure(figsize=(16,8))
    plot(cluster_amounts, metrics, 'bx-')
    xlabel('Number of Clusters')
    ylabel(name_of_metric)
    title('The ' + name_of_metric + ' for cluster counts from ' + str(cluster_amounts[0]) + ' to ' + str(cluster_amounts[-1]))

def cluster_and_draw_plot(data : DataFrame, cluster_amounts : List[int], elbow_method : bool = True, silhouette_coefficient : bool = True):
    metrics = clustering_with_metrics(data, cluster_amounts, elbow_method, silhouette_coefficient)
    if elbow_method and silhouette_coefficient:
        elbow_metrics = metrics[0]
        silhouette_metrics = metrics[1]
        draw_plot(cluster_amounts, elbow_metrics, "Elbow Method")
        draw_plot(cluster_amounts, silhouette_metrics, "Silhouette Coefficient")
    elif elbow_method:
        draw_plot(cluster_amounts, metrics, "Elbow Method")
    elif silhouette_coefficient:
        draw_plot(cluster_amounts, metrics, "Silhouette Coefficient")

def create_new_material_row_for_prediction(substance_identifiers: List[str], substance_amounts: List[str], all_substance_identifiers: List[str]) -> List[List[Decimal]]:
    identifiers_to_index = {key: value for value, key in enumerate(all_substance_identifiers)}
    new_row = [Decimal(0)] * len(all_substance_identifiers)
    total_weight = sum(substance_amounts)
    if total_weight == 0:
        return [new_row]
    for i in range(len(substance_identifiers)):
        # print(str(substance_identifiers[i]))
        if substance_identifiers[i] != substance_identifiers[i] or substance_identifiers[i] == '': 
            continue
        if substance_identifiers[i] in identifiers_to_index:
            index = identifiers_to_index[substance_identifiers[i]]
            new_row[index] += substance_amounts[i] / total_weight
        else:
            pass
            # index = identifiers_to_index[UNKNOWN_SUBSTANCE_IDENTIFIER]
            # currently, unknown materials are not part of clustering!
        

    new_material = [new_row]
    return new_material

def predict_material_clusters(component: Tuple[str], model: KMeans, all_substance_identifiers: List[str]) -> List[int]:
    material_clusters = []
    material_info = get_material_info_for_prediction(component)
    for material in material_info:
        new_material = create_new_material_row_for_prediction(*material, all_substance_identifiers)
        # TODO: what to do with multiple predicted cluster numbers?
        cluster_number = model.predict(new_material)[0]
        material_clusters.append(str(cluster_number))
    # print(material_clusters)
    return material_clusters

def get_material_info_for_prediction(component: Tuple[str]) -> List[Tuple[List[str]]]:
    # print(component)
    _, _, _, _, _, substance_counts, _, substance_identifiers, substance_amounts = component
    if (substance_amounts != substance_amounts or substance_counts != substance_counts or substance_identifiers != substance_identifiers):
        # one of the inputs is nan and we cannot work with it
        return []
    substance_identifiers = str(substance_identifiers).split(LIST_DELIMITER)
    substance_amounts = [Decimal(amount) for amount in str(substance_amounts).split(LIST_DELIMITER)]
    substance_counts = [int(count) for count in str(substance_counts).split(LIST_DELIMITER)]
    # print(substance_identifiers, substance_amounts, substance_counts)
    material_info = []
    for i in range(len(substance_counts)):
        num_substances = substance_counts[i]
        start_index = sum(substance_counts[:i])
        material_substance_identifiers = substance_identifiers[start_index : start_index + num_substances]
        material_substance_amounts = substance_amounts[start_index : start_index + num_substances]
        material_info.append((material_substance_identifiers, material_substance_amounts))
    return material_info