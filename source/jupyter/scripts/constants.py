# paths and directories
NXP_DATA_DIRECTORY = "../scrapy/data/nxp/"
TE_DATA_DIRECTORY = "../scrapy/data/te_connectivity/"
COMPONENTS_FOLDER = "csv/components/"
MATERIALS_FOLDER = "csv/homogeneous_materials/"
SUBSTANCES_FOLDER = "csv/substances/"
SUBSTANCES_JSON_FOLDER = "json/substances/"
MATERIAL_MODELS_FOLDER = "clustering_models/homogeneous_materials/"
COMPONENT_MODELS_FOLDER = "clustering_models/components/"
COMPONENT_JSON_FOLDER = "json/components/"

# header for CSV files
SUBSTANCE_CSV_HEADER = ["name", "authority", "identity"]
MATERIALS_CSV_HEADER = ["name", "material_group_name"]
MATERIAL_COMPOSITION_CSV_HEADER = ['material_name', 'substance_list', 'identifier_list', 'amounts_list']
COMPONENT_COMPOSITION_CSV_HEADER = ['name', 'part_no', 'manufacturer', 'materials', 'material_amounts', 'substance_counts', 'substances', 'substance_identifiers', 'substance_amounts']

# Strings for unknown substances or materials
UNKNOWN_SUBSTANCE_IDENTIFIER = '9999999-99-9'
UNKNOWN_SUBSTANCE_NAME = 'Unknown'

# Delimiters for writing and reading CSVs
LIST_DELIMITER = '|'
DELIMITER = '!'

# for converting numbers with metric
from decimal import Decimal 
METRIC_MODIFIER_TO_FACTOR = {
    "p": Decimal(1)/Decimal(10**12),
    "n": Decimal(1)/Decimal(10**9),
    "µ": Decimal(1)/Decimal(10**6),
    "m": Decimal(1)/Decimal(10**3),
    "": 1,
    "k": 10**3,
    "M": 10**6,
    "G": 10**9,
    "T": 10**12
}

MANUFACTURERS = {"NXP Semiconductors" : {"name" : "NXP Semiconductors", "short": "NXP", "part_id":"itemName"}, 
                "TE Connectivity" : {"name": "TE Connectivity", "short": "TE", "part_id":"itemNumber"}}