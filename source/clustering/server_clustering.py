import sys, os, time, re
from typing import Any, List, Dict

from numpy import delete

run_mode = 'CPU'                          # uncomment this line if running on CPU
# run_mode = 'GPU'                            # uncomment this line if running on GPU

if run_mode == 'CPU':
    from pandas import read_csv, DataFrame
    from sklearn.cluster import KMeans
    from sklearn.metrics import silhouette_score
elif run_mode == 'GPU':
    import cudf
    from cudf import read_csv, DataFrame
    from cuml.cluster import KMeans
    from cuml.metrics.cluster.silhouette_score import cython_silhouette_score as silhouette_score

UNKNOWN_SUBSTANCE_IDENTIFIER = '9999999-99-9'

def load_materials(input_file: str) -> DataFrame:
    if not os.path.isfile(input_file):
        print("CSV input file for data does not exist, please add", input_file, "to the current directory. Aborting...")
        sys.exit(1)

    materials = read_csv(input_file, index_col = [0, 1])
    return materials

def load_components(input_file: str) -> DataFrame:
    if not os.path.isfile(input_file):
        print("CSV input file for data does not exist, please add", input_file, "to the current directory. Aborting...")
        sys.exit(1)

    components = read_csv(input_file, index_col = [0, 1, 2])
    return components

# potentially with and without unknown substances
experiments = {
    'materials': [
        {'name': 'with_unknown_substances', 'file': 'data/materials_with_unknown_to_cluster.csv', 'load': load_materials},
        {'name': 'without_unknown_substances', 'file': 'data/materials_without_unknown_to_cluster.csv', 'load': load_materials},
    ],
    'components': [
        {'name': 'by_23_clustered_materials', 'file': 'data/components_to_cluster_by_23_materials.csv', 'load': load_components}, 
        {'name': 'by_127_clustered_materials', 'file': 'data/components_to_cluster_by_127_materials.csv', 'load': load_components},
        {'name': 'by_substances', 'file': 'data/components_to_cluster_by_substances.csv', 'load': load_components},
    ]
}

def cluster(data, cluster_amounts : List[int], path: str):
    elbow_metrics = open(path + 'elbow_metrics.csv', 'a')
    silhouette_metrics = open(path + 'silhouette_metrics.csv', 'a')
    log_file = open(path + 'log.csv', 'a')

    for k in cluster_amounts:
        print("Clustering into", k, "clusters...")
        kmeanModel = KMeans(n_clusters=k, random_state=1)
        start_time = time.time()
        kmeanModel.fit(data)
        k = str(k)
        cluster_time = time.time() - start_time
        
        # save metrics together with cluster number as tuple
        silhouette_metrics.write(','.join([k, str(silhouette_score(data, kmeanModel.labels_, metric='euclidean'))]) + '\n')
        elbow_metrics.write(','.join([k, str(kmeanModel.inertia_)]) + '\n')
        log_file.write(','.join([k, str(cluster_time), str(time.time() - start_time)]) + '\n')      

        # save kmeanModel.labels_ as csv file per clustering with material/component Ids
        clustering_df = data.index.to_frame(index = False)
        clustering_df.set_index(0, inplace = True)
        clustering_df['cluster'] = kmeanModel.labels_
        clustering_df.to_csv(path + 'cluster_labels_' + k + '.csv')

    # save metrics in a file per try
    elbow_metrics.close()
    silhouette_metrics.close()
    log_file.close()

def extract_numbers_from_csv_filenames(filenames: List[str]) -> List[int]:
    numbers = []
    for filename in filenames:
        match = re.match(r'.*_(\d+).csv$', filename)
        if match:
            number = int(match.group(1))
            numbers.append(number)
    return numbers

def extract_numbers_from_filenames(filenames: List[str]) -> List[int]:
    numbers = []
    for filename in filenames:
        match = re.match(r'^\d+$', filename)
        if match:
            numbers.append(int(filename))
    return numbers

def run_experiments(path: str, args: List[str]):
    # path = 'clusterings'
    domain = args['domain']
    domain_path = path + domain + '/'
    for experiment in experiments[domain]:
        experiment_path = domain_path + experiment['name'] + '/'
        continue_try = args['continue']
        retry = args['retry']
        if not args['try_number']:
            try_number = infer_try_number(experiment_path)
        else:
            try_number = args['try_number']
            if not os.path.isdir(experiment_path + '/' + args['try_number']):
                print(f"Clustering try no. {args['try_number']} doesn't exist as directory in {experiment_path}, treating as new try...")
                continue_try = False
                retry = False
        
        try_path = experiment_path + try_number + '/'

        # continue or retry
        if continue_try:
            print("Continuing clustering no.", try_number, "...")
            start_clustering = get_next_cluster_number(try_path)
        elif retry:
            print("Retrying clustering no.", try_number, "...")
            delete_try(try_path)
            prepare_new_try(try_path)
            start_clustering = 2
        else:
            print("Creating folders and files for new try", try_number, "...")
            prepare_new_try(try_path)
            start_clustering = 2

        # load data
        data = experiment['load'](experiment['file'])

        end_clustering = len(data) // 2

        if start_clustering >= end_clustering:
            print("Try is already complete, skipping...")
            continue
        
        print("Starting experiment", experiment['name'], "...")
        number_of_clusters = range(start_clustering, end_clustering, 1)
        cluster(data, number_of_clusters, try_path)

def prepare_new_try(path: str):
    # path = 'clusterings/<domain>/<experiment>/<try>/'
    create_try_directory(path)
    create_metric_files(path)

def create_try_directory(path: str):
    # path = 'clusterings/<domain>/<experiment>/<try>/'
    os.mkdir(path)

def create_metric_files(path: str):
    # path = 'clusterings/<domain>/<experiment>/<try>/'
    with open(path + 'elbow_metrics.csv', 'w') as f:
        f.write('number of clusters,intertia\n')
    with open(path + 'silhouette_metrics.csv', 'w') as f:
        f.write('number of clusters,silhouette coefficient\n')
    with open(path + 'log.csv', 'w') as f:
        f.write('number of clusters,cluster time,metric calculation time\n')

def delete_try(path: str):
    # path = 'clusterings/<domain>/<experiment>/<try>/'
    for file in os.listdir(path):
        os.remove(path+file)
    os.rmdir(path)

def get_next_cluster_number(path: str):
    # path = 'clusterings/<domain>/<experiment>/<try>/'
    # see what the last clustering was, then continue from that number
    numbers = extract_numbers_from_csv_filenames(os.listdir(path))
    last_clustering_number = max(numbers, default = 1)
    next_cluster_number = last_clustering_number + 1
    return next_cluster_number

def parse_cmd_args(args: List[str]) -> Dict[str, Any]:
    if len(args) < 2:
        print(f"Usage: $ python3 {args[0]} -d materials|components [-c <try number> | -r <try number>]")
        print(f"-d: data option, which data to cluster: materials or components")
        print("-c: continue previously started try with specific try number")
        print("-r: retry previously made try with specific try number")
        sys.exit(1)

    parsed_args = {'domain': None, 'continue': False, 'retry': False, 'try_number' : None}
    
    try:
        domain = args[args.index('-d') + 1]
    except:
        raise SystemExit("Did not specify which data to use, call script with -d materials|components")
    parsed_args['domain'] = domain

    if domain not in experiments.keys():
        raise SystemExit(f"Specified domain {domain} is not included, please use one of: {list(experiments.keys())}.")

    continue_try = False
    retry = False
    try_number = None

    if '-c' in args:
        continue_try = True
        try:
            try_number = args[args.index('-c') + 1]
        except:
            raise SystemExit("Did not specify, which try should be continued, call script with -c <try number>")
    if '-r' in args:
        retry = True
        try:
            try_number = args[args.index('-r') + 1]
        except:
            raise SystemExit("Did not specify, which try should be retried, call script with -r <try number>")
    if continue_try and retry:
        raise SystemExit("Please only use either the continue or retry option, not both.")

    parsed_args['continue'] = continue_try
    parsed_args['retry'] = retry
    if try_number:
        parsed_args['try_number'] = try_number

    return parsed_args

def prepare_directories(args: Dict[str, Any]):
    if not os.path.isdir('clusterings'):
        os.mkdir('clusterings')

    domain = args['domain']
    if not os.path.isdir('clusterings/' + domain):
        os.mkdir('clusterings/' + domain)

    for experiment in experiments[domain]:
        if not os.path.isdir('clusterings/' + domain + '/' + experiment['name']):
            os.mkdir('clusterings/' + domain + '/' + experiment['name'])

def infer_try_number(experiment_path: str):
    tries = os.listdir(experiment_path)
    if len(tries) == 0:
        try_number = str(1)
    else:
        numbers = extract_numbers_from_filenames(tries)
        try_number = str(max(numbers, default = 0) + 1)
        
    if not try_number:
        raise SystemExit("Could not infer try number, aborting...")
    return try_number

if __name__ == '__main__':
    args = parse_cmd_args(sys.argv)
    prepare_directories(args)
    run_experiments('clusterings/', args)
    print("Finishing...")

