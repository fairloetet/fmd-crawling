from abc import ABC, abstractmethod
from decimal import Decimal

import numpy as np


class Attribute(ABC):
    @abstractmethod
    def similarity(self, value1, value2):
        """Computes the similarity of two values.
        
        :returns: A float ranging from 0 to 1 giving the similarity.
        """
        raise NotImplemented('Attribute.similarity')


class CategoricalAttribute(Attribute):
    def similarity(self, value1, value2):
        """Checks whether both values are identical."""
        return int(value1 == value2)


def get_quotient(value1, value2):
    # divide smaller value by larger value
    if np.isclose(value1, value2, rtol=1e-05, atol=1e-08, equal_nan=False):
        return 1
    if value1 < value2:
        if np.isclose(value2, 0, rtol=1e-05, atol=1e-08, equal_nan=False):
            return 0
        return value1/value2
    else:
        if np.isclose(value1, 0, rtol=1e-05, atol=1e-08, equal_nan=False):
            return 0
        return value2/value1


class NumericAttribute(Attribute):
    def similarity(self, value1, value2):
        """Compares two positive numbers by computing their ratio."""
        value1 = int(value1)
        value2 = int(value2)
        return get_quotient(value1, value2)


METRIC_MODIFIER_TO_FACTOR = {
    "p": Decimal(1)/Decimal(10**12),
    "n": Decimal(1)/Decimal(10**9),
    "µ": Decimal(1)/Decimal(10**6),
    "m": Decimal(1)/Decimal(10**3),
    "": 1,
    "k": 10**3,
    "M": 10**6,
    "G": 10**9,
    "T": 10**12
}


def convert_metrics(value: str, base_metric: str) -> float:
    number, metric = value.split(" ")
    number = Decimal(number)
    if metric != base_metric:
        if len(metric) > 1 and metric[0] in METRIC_MODIFIER_TO_FACTOR and metric[1:] == base_metric:
            modifier = metric[0]
            factor = METRIC_MODIFIER_TO_FACTOR[modifier]
            number *= factor
        else:
            print("Unknown metric or modifier in", value)
    
    if int(number) == float(number):
        return int(number)
    else:
        return float(number)
    

class NumericUnitAttribute(Attribute):
    def __init__(self, base_unit, observed_range):
        self.base_unit = base_unit
        self.observed_range = observed_range

    def similarity(self, value1, value2):
        # convert into comparable numbers, remove metric unit
        # TODO: not convert if both have the same modifier
        value1 = convert_metrics(value1, self.base_unit)
        value2 = convert_metrics(value2, self.base_unit)
        
        # scale both values if the minimal value of this attribute is below zero (e.g. for temperatures)
        min_of_attr = float(self.observed_range[0])
        if min_of_attr < 0:
            value1 = value1 - min_of_attr
            value2 = value2 - min_of_attr
        
        # Clip to 0
        return max(0.0, get_quotient(value1, value2))


class ListOfCategoriesAttribute(Attribute):
    def __init__(self, separator: str = ', '):
        self.separator = separator

    def similarity(self, value1, value2):
        # intersection / union of values
        value1 = value1.split(self.separator)
        value2 = value2.split(self.separator)
        len_intersection = len(set(value1).intersection(value2))
        # print("LOC:", len_intersection, len(value1)+len(value2), value1, ",", value2)
        return len_intersection / (len(value1) + len(value2) - len_intersection)
