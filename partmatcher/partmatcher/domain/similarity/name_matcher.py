import Levenshtein

class NameMatcher:
    """A minimal similarity metric that just compares two components’ names."""
    def __init__(self, part_api):
        self.part_api = part_api

    def __call__(self, part1: str, part2: str):
        part1_data = self.part_api.lookup(part_no=part1)
        part2_data = self.part_api.lookup(part_no=part2)
        if not part1_data or not part2_data:
            return 0.0
        return Levenshtein.ratio(part1_data.get('name', ''), part2_data.get('name', ''))
