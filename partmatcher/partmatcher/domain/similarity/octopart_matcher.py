from decimal import Decimal
from numpy import isclose
import re
from typing import Dict

from . import attribute


company_to_short = {
    'TE Connectivity' : "TE",
    'TE Connectivity / AMP' : "TE", 
    'TE Connectivity / Raychem' : "TE", 
    'TE Connectivity / Alcoswitch' : "TE",
    'TE Connectivity / DEUTSCH' : "TE", 
    'TE Connectivity / Holsworthy' : "TE",
    'TE Connectivity / Citec' : "TE", 
    'TE Connectivity / Corcom' : "TE", 
    'OEG' : "TE",
    'NXP Semiconductors' : 'NXP',
    'Nexperia' : "NXP", 
    'Freescale Semiconductor' : "NXP"}


METRIC_MODIFIER_TO_FACTOR = {
    "p": Decimal(1)/Decimal(10**12),
    "n": Decimal(1)/Decimal(10**9),
    "µ": Decimal(1)/Decimal(10**6),
    "m": Decimal(1)/Decimal(10**3),
    "": 1,
    "k": 10**3,
    "M": 10**6,
    "G": 10**9,
    "T": 10**12
}

CAT = "Categorical"
NUM = "Numerical"
NWMU = "Numerical with Metric Units"
LOC = "List of Categories"

class OctopartMatcher:
    def __init__(self, octopart_repo, fields: Dict[str, attribute.Attribute]):
        self.repo = octopart_repo
        self.fields = fields

    @classmethod
    def from_octopart_repo(cls, repo):
        attr_to_type, attr_to_values, attr_to_metric = infer_attribute_types(repo)
        base_metrics = collect_base_metrics(attr_to_type, attr_to_metric)
        fields = {}
        fields['short_description'] = attribute.ListOfCategoriesAttribute(separator=' ')
        for attr_name in attr_to_type:
            value_type = attr_to_type[attr_name]
            if value_type == CAT:
                fields[attr_name] = attribute.CategoricalAttribute()
            elif value_type == NUM:
                fields[attr_name] = attribute.NumericAttribute()
            elif value_type == NWMU:
                min_of_attr = min(attr_to_values[attr_name])
                max_of_attr = max(attr_to_values[attr_name])
                fields[attr_name] = attribute.NumericUnitAttribute(base_metrics[attr_name], (min_of_attr, max_of_attr))
            elif value_type == LOC:
                fields[attr_name] = attribute.ListOfCategoriesAttribute()
        return cls(repo, fields)

    def compare_attr(self, attr_name, value1, value2):
        return self.fields[attr_name].similarity(value1, value2)
        
    def compare_octoparts(self, octo1, octo2):
        dict1 = preprocess_oct_dict(octo1)
        dict2 = preprocess_oct_dict(octo2)
        
        intersection_attributes = 0
        union_attributes = len(dict1) + len(dict2)
        similarity = 0
        if union_attributes == 0:
            # both have no attributes so similarity cannot be determined correctly
            # but if comparing a part to itself, it should be 1
            return 1
        
        compared = 0
        # Compare descriptions
        if 'short_description' in octo1 and 'short_description' in octo2:
            attr_sim = self.compare_attr('short_description', octo1['short_description'], octo2['short_description'])
            similarity += attr_sim
            compared += 1

        # Compare tech specs
        for key in dict1.keys():
            if key in dict2:
                #compare both attributes
                attr_sim = self.compare_attr(key, dict1[key], dict2[key])
                similarity += attr_sim
                compared += 1
                
        union_attributes -= intersection_attributes
        unique_attributes = set(dict1.keys()).union(set(dict2.keys()))
        scaling_factor = len(unique_attributes) + 1  # add one to account for short_description
        
        # print("End Sim:", similarity, intersection_attributes, union_attributes)
        if compared > 0:
            similarity /= scaling_factor
        return similarity

    def __call__(self, part1: str, part2: str):
        part1_data = self.repo.lookup(part_no=part1)
        part2_data = self.repo.lookup(part_no=part2)
        if not part1_data or not part2_data:
            return 0.0
        return self.compare_octoparts(part1_data, part2_data)


def convert_metrics(value: str, base_metric: str) -> float:
    number, metric = value.split(" ")
    number = Decimal(number)
    if metric != base_metric:
        if len(metric) > 1 and metric[0] in METRIC_MODIFIER_TO_FACTOR and metric[1:] == base_metric:
            modifier = metric[0]
            factor = METRIC_MODIFIER_TO_FACTOR[modifier]
            number *= factor
        else:
            print("Unknown metric or modifier in", value)
    
    if int(number) == float(number):
        return int(number)
    else:
        return float(number)
        
    return (results, base_metric)


def preprocess_oct_dict(oct_dict):
    new_dict = {}
    for attribute in oct_dict['specs']:
        new_dict[attribute['attribute']['name']] = attribute['display_value']
    return new_dict


def get_quotient(value1, value2):
    # divide smaller value by larger value
    if isclose(value1, value2, rtol=1e-05, atol=1e-08, equal_nan=False):
        return 1
    if value1 < value2:
        if isclose(value2, 0, rtol=1e-05, atol=1e-08, equal_nan=False):
            return 0
        return value1/value2
    else:
        if isclose(value1, 0, rtol=1e-05, atol=1e-08, equal_nan=False):
            return 0
        return value2/value1


def get_difference_in_range(value1, value2, min_value, max_value):
    # 1 - diff / max-min of attribute
    return 1 - abs(value1 - value2) / (max_value - min_value)


def collect_tech_specs(octopart_repo):
    counts = {"companion_products" : [], "datasheets" : [], "descriptions" : [], "images": [], "reference_designs" : [], "similar_parts": [], "specs" : []}
    tech_specs = {}
    for part in octopart_repo.iterparts():
        for counted in part["counts"].keys():
            counts[counted].append(part["counts"][counted])
        for spec in part["specs"]:
            attribute = spec["attribute"]
            attr_name = attribute["name"]
            if not attr_name in tech_specs:
                tech_specs[attr_name] = {}
                tech_specs[attr_name]["group"] = attribute["group"]
                tech_specs[attr_name]["values"] = []
                tech_specs[attr_name]["count"] = 0
            tech_specs[attr_name]["values"].append(spec["display_value"])
            tech_specs[attr_name]["count"] += 1
    return tech_specs


def collect_attributes(tech_specs):
    specs_with_counts = [(tech_specs[key]["count"], key) for key in tech_specs.keys()]
    attribute_names = [x[1] for x in sorted(specs_with_counts, reverse = True)]# [:25]]
    return attribute_names


def collect_part_attributes(octopart_repo, attribute_names):
    part_attributes = {}
    for part in octopart_repo.iterparts():
        manufacturer_name = part['manufacturer']['name']
        if manufacturer_name in company_to_short:
            cmp_short = company_to_short[manufacturer_name]
            key = cmp_short + '_' + part["mpn"]
            part_attributes[key] = {}
            for spec in part["specs"]:
                attribute = spec["attribute"]
                if attribute["name"] in attribute_names:
                    part_attributes[key][attribute["name"]] = spec["display_value"]
        else:
            print(part['manufacturer']['name'])
    return part_attributes


def infer_attribute_types(octopart_repo):
    tech_specs = collect_tech_specs(octopart_repo)
    attribute_names = collect_attributes(tech_specs)
    part_attributes = collect_part_attributes(octopart_repo, attribute_names)

    attr_to_type = {}
    attr_to_values = {}
    attr_to_metric = {}
    for attribute_name in attribute_names:
        guess = None
        attr_to_values[attribute_name] = set()
        for part in part_attributes.keys():
            if attribute_name in part_attributes[part].keys():
                part_attr = part_attributes[part][attribute_name]
                
                # matching integers, not floats!
                if re.match("^0$|^([1-9]\d*)$", part_attr):
                    current_guess = NUM
                # matching pos/neg integers or floats with metric units from 1 - 3 letters
                elif re.match("^[-+]?[0-9]*[\.|,]?[0-9]+ [° µ Ω % A-Z a-z /]{1,7}$", part_attr):
                    current_guess = NWMU
                # matching comma and space separated lists of digit-letter combinations
                # could also try to split at ", " 
                elif re.match("^([A-Z a-z 0-9 \- / \|]+, )+[A-Z a-z 0-9 \- / \|]*$", part_attr):
                    current_guess = LOC
                else: 
                    current_guess = CAT

                if not guess:
                    guess = current_guess
                
                # looking whether guess needs to be overridden
                if guess != current_guess:
                    if guess == CAT and current_guess == LOC:
                        print("Overriding CAT because of", part_attr)
                        guess = LOC
                    elif guess == NUM and current_guess == CAT:
                        print("Overriding NUM because of", part_attr)
                        guess = CAT
                        # override data types in set
                        attr_to_values[attribute_name] = set([str(value) for value in list(attr_to_values[attribute_name])])
                    elif guess == NUM and current_guess == LOC:
                        print("Overriding NUM because of", part_attr)
                        guess = LOC
                        # override data types in set
                        attr_to_values[attribute_name] = set([str(value) for value in list(attr_to_values[attribute_name])])
                    elif guess == LOC and current_guess == NUM or guess == LOC and current_guess == CAT or guess == CAT and current_guess == NUM:
                        pass
                    else:
                        print(guess, "does not match", current_guess, "for", part_attr, attribute_name)

                if guess == NUM:
                    value = [int(part_attr)]
                elif guess == NWMU:
                    value = [part_attr.split(" ")[0]]
                    metric = [part_attr.split(" ")[1]]
                    if not attribute_name in attr_to_metric.keys():
                        attr_to_metric[attribute_name] = set()
                    attr_to_metric[attribute_name].update(metric)
                elif guess == CAT:
                    value = [part_attr]
                elif guess == LOC:
                    value = part_attr.split(", ")
                
                attr_to_values[attribute_name].update(value)
                
        attr_to_type[attribute_name] = guess
        if not guess:
            del attr_to_values[attribute_name]
        print(guess, "for", attribute_name)
    return attr_to_type, attr_to_values, attr_to_metric

def collect_base_metrics(attr_to_type, attr_to_metric):
    # extract base metrics from attr with metric units
    # take smallest metric string from all that occur -> base metric
    # or if multiple have same length, but nothing is smallest, check whether all prefixes are modifiers, then basemetric without modifiers
    base_metrics = {}
    for attr, atype in attr_to_type.items():
        if atype == NWMU:
            values = attr_to_metric[attr]
            if len(values) == 1:
                # if we only have one metric, take it as base metric and never convert
                metric = list(values)[0]
                print("Only", metric, "for", attr)
            else:
                if len(set(len(string) for string in values)) > 1:
                    # if we have multiple metrics with different lengths, the shortest must be the base metric
                    metric = min(values, key=len)
                    print("Shortest", metric, "for", attr)
                else:
                    # we have multiple metrics all with the same lengths, which means they all have different modifiers
                    metrics = set(string[1:] for string in values)
                    if len(metrics) == 1:
                        metric = list(metrics)[0]
                        print("Base", metric, "for", attr)
                    else:
                        raise Exception("Something went wrong trying to figure out the base metric for", attr, "with", values)
            if metric == "":
                raise Exception("Something went wrong splitting metric and modifier, resulting metric is empty for", attr)
            base_metrics[attr] = metric
    return base_metrics