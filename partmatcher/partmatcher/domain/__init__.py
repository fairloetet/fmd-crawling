from dataclasses import dataclass, field
from typing import List, Optional

import numpy as np


@dataclass
class Substance:
    """Represents a substance by its CAS number."""
    # The CAS registry number of the substance
    cas_number: str


@dataclass
class MappedComponent:
    """A representation of the inferred properties of a component.
    
    Our goal is to produce a MappedComponent instance for any given input component name.
    """
    # Component’s part number, used for identification and matching
    part_no: str
    # Material share per substance
    composition: np.array
    # Mass in g
    mass: float

    # Optional name of the component
    name: Optional[str] = None

    @property
    def mass_by_substance(self) -> np.array:
        return self.mass * self.composition


@dataclass
class ComponentClustering:
    clusters: List['ComponentCluster']

    @property
    def items(self):
        for cluster in self.clusters:
            for item in cluster:
                yield item

@dataclass
class ComponentCluster:
    # The components that are in the cluster
    members: List[MappedComponent]

    # Optional subset of components that are representative for the clustering
    # These do not have to be members! E.g. they could be aggregated from member values.
    representatives: List[MappedComponent] = field(default_factory=list)
    # Optional ID to identify the cluster
    cluster_id: Optional[int] = None
