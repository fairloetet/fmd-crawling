from .clustering_mapper import ClusteringMapper
from .direct_mapper import DirectMapper
from .random_mapper import RandomMapper