import numpy as np
import pandas as pd
from typing import Callable, List

from ... import domain


Metric = Callable[[domain.MappedComponent, domain.MappedComponent], float]


class EvaluationContext:
    def __init__(self, test_data: List[domain.MappedComponent], metrics: List[Metric]):
        self.test_data = test_data
        self.metrics = metrics

    def evaluate_mapper(self, mapper) -> pd.DataFrame:
        """Returns a data frame of evaluation results for each test component."""
        return pd.DataFrame([
            {metric.__name__: metric(mapper(test_component), test_component)
             for metric in self.metrics}
            for test_component in self.test_data
        ])


def absolute_error(mapped: domain.MappedComponent, true: domain.MappedComponent) -> float:
    """Accumulates the substance-wise prediction errors, scaled to the true
    mass.

    An error of 0 is perfect, and an error of 1 means that the amount of
    incorrectly predicted substances equals the component mass.

    :param mapped: the predicted mapping
    :param true: the actual material composition of the component
    :return: share of correctly predicted material
    """
    return np.sum(np.abs(mapped.mass_by_substance - true.mass_by_substance)) / true.mass


def correct_mass_percentage(mapped: domain.MappedComponent, true: domain.MappedComponent) -> float:
    """Gives a percentage for the correctness of a mapping.
    
    This is the minimum of the following two quantities:

    1. The percentage of predicted substances that are actually in the true component
    2. The percentage of substances of the true component that are predicted

    The score ranges from 0 to 1 (perfect).
    
    :param mapped: the predicted mapping
    :param true: the actual material composition of the component
    :return: share of correctly predicted material
    """
    excess_masses = np.fmax(mapped.mass_by_substance - true.mass_by_substance, 0)
    deficient_masses = np.fmax(true.mass_by_substance - mapped.mass_by_substance, 0)
    return 1.0 - max(np.sum(excess_masses) / mapped.mass, np.sum(deficient_masses) / true.mass)
