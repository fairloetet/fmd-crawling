from dataclasses import dataclass
from enum import Enum
from typing import Callable

from ... import domain

class ClusteringMapper:
    class CompareTo(Enum):
        ALL_MEMBERS = 1
        REPRESENTATIVES = 2

    def __init__(self,
                 clustering: domain.ComponentClustering,
                 similarity_metric: Callable[[str, str], float],
                 compare_to=CompareTo.REPRESENTATIVES):
        self.clustering = clustering
        self.similarity_metric = similarity_metric
        self.compare_to = compare_to

    def __call__(self, part_no: str) -> domain.MappedComponent:
        # Calculate similaritiy to each relevant component
        @dataclass
        class ScoredMember:
            member: domain.MappedComponent
            cluster: int
            score: float

        if self.compare_to == self.CompareTo.ALL_MEMBERS:
            compare_items = lambda cluster: cluster.members
        elif self.compare_to == self.CompareTo.REPRESENTATIVES:
            compare_items = lambda cluster: cluster.representatives

        similarity_scored = [
            ScoredMember(member=mapped_component, cluster=cluster.cluster_id, score=self.similarity_metric(part_no, mapped_component.part_no))
            for cluster in self.clustering.clusters for mapped_component in compare_items(cluster)
        ]

        best_match = max(similarity_scored, key=lambda v: v.score)

        # Directly return the best matching component
        return best_match.member
    