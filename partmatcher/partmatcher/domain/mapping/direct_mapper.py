from dataclasses import dataclass
from enum import Enum
from typing import Callable

from ... import domain

class DirectMapper:
    """Compares a query component to every component in the clustering and finds component with the highest similarity score."""
    def __init__(self,
                 clustering: domain.ComponentClustering,
                 similarity_metric: Callable[[str, str], float]):
        self.clustering = clustering
        self.similarity_metric = similarity_metric

    def __call__(self, part_no: str) -> domain.MappedComponent:
        # Calculate similaritiy to each relevant component
        @dataclass
        class ScoredMember:
            member: domain.MappedComponent
            cluster: int
            score: float

        similarity_scored = [
            ScoredMember(member=mapped_component, cluster=cluster.cluster_id, score=self.similarity_metric(part_no, mapped_component.part_no))
            for cluster in self.clustering.clusters for mapped_component in cluster.members
        ]

        best_match = max(similarity_scored, key=lambda v: v.score)

        # Directly return the best matching component
        return best_match.member
    