import random
from typing import List

import numpy as np

from ... import domain

class RandomMapper:
    """Maps components by wildly guessing their weight and composition."""
    def __init__(self, substances: List[domain.Substance], rnd = random.Random(42)):
        self.substances = substances
        self.rnd = rnd

    def __call__(self, part_no: str) -> domain.MappedComponent:
        # Create a random, normalized material distribution
        materials = np.array([self.rnd.expovariate(1.0) for _ in self.substances])
        materials = materials / sum(materials)
        return domain.MappedComponent(
            part_no=part_no,
            mass=self.rnd.gammavariate(2.0, 2.0), # Guess the weight (expectation is 2/2=1)
            composition=materials
        )
