from pathlib import Path

from partmatcher import domain
from partmatcher.infrastructure.csv_data import load_clustering

def test_load_clustering():
    clustering = load_clustering(Path('../source/jupyter/csv/components/substance_clustering_initial.csv'))
    assert isinstance(clustering, domain.ComponentClustering)
    assert len(clustering.clusters) == 100
    
