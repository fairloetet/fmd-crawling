import pytest

from partmatcher.domain.similarity.compare import *


def test_categorical_attribute():
    attribute = CategoricalAttribute()
    assert attribute.similarity(1, 1) == 1
    assert attribute.similarity(0, 0) == 1
    assert attribute.similarity('1.0', '1.0') == 1
    assert attribute.similarity(1, '1') == 0
    assert attribute.similarity(1, None) == 0


def test_numeric_attribute():
    attribute = NumericAttribute()
    assert attribute.similarity(1.0, 1.0) == pytest.approx(1.0)
    assert attribute.similarity(1, 1) == pytest.approx(1)
    assert attribute.similarity(0.0, 1.0) == pytest.approx(0)
    assert attribute.similarity(2.0, 1.0) == pytest.approx(0.5)
    assert attribute.similarity(1.0, 2.0) == pytest.approx(0.5)
    assert attribute.similarity(1.0, 3.0) == pytest.approx(1.0/3.0)
    assert attribute.similarity(0.0, 0.0) == pytest.approx(1.0)


def test_numeric_unit_attribute():
    attribute = NumericUnitAttribute(base_unit='g', observed_range=(0.0, 10.0))
    assert attribute.similarity('1.0 g', '3.0 g') == pytest.approx(1.0/3.0)
    assert attribute.similarity('3.0 g', '1.0 g') == pytest.approx(1.0/3.0)
    assert attribute.similarity('0.0 g', '1.0 g') == pytest.approx(0.0)
    assert attribute.similarity('0.1 g', '10.0 g') == pytest.approx(0.01)
    # outside observed range
    assert attribute.similarity('-1.0 g', '3.0 g') == pytest.approx(0.0)
    assert attribute.similarity('1.0 kg', '3.0 kg') == pytest.approx(1.0/3.0)
    assert attribute.similarity('1.0 g', '1.0 kg') == pytest.approx(0.001)


def test_numeric_unit_attribute_negative():
    attribute = NumericUnitAttribute(base_unit='°', observed_range=(-10.0, 10.0))
    assert attribute.similarity('5.0 °', '10.0 °') == pytest.approx(15.0 / 20.0)
    assert attribute.similarity('10.0 °', '5.0 °') == pytest.approx(15.0 / 20.0)
    assert attribute.similarity('15.0 °', '5.0 °') == pytest.approx(15.0 / 25.0)
    assert attribute.similarity('-5.0 °', '5.0 °') == pytest.approx(5.0 / 15.0)
    assert attribute.similarity('-5.0 °', '0.0 °') == pytest.approx(5.0 / 10.0)


def test_list_of_categories_attribute():
    attribute = ListOfCategoriesAttribute()
    assert attribute.similarity('A, B', 'A, C') == pytest.approx(1.0/3.0)
    assert attribute.similarity('A', 'A') == pytest.approx(1.0)
    assert attribute.similarity('A', 'B') == pytest.approx(0.0)
    assert attribute.similarity('A, B, C', 'A, C, D') == pytest.approx(0.5)


def test_list_of_categories_tokens():
    attribute = ListOfCategoriesAttribute(separator=' ')
    assert attribute.similarity('A B', 'A C') == pytest.approx(1.0/3.0)
    assert attribute.similarity('A', 'A') == pytest.approx(1.0)
    assert attribute.similarity('A', 'B') == pytest.approx(0.0)
    assert attribute.similarity('A B C', 'A C D') == pytest.approx(0.5)
