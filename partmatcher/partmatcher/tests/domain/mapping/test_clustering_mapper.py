import numpy as np

from partmatcher import domain
from partmatcher.domain.mapping import ClusteringMapper

def test_clustering_mapper():
    similarity_scores = {
        ('search', 'part1'): 0.8,
        ('search', 'part2'): 0.4,
        ('search', 'part3'): 0.2,
        ('search', 'part4'): 0.95,
    }
    def similarity_function(part1, part2):
        return similarity_scores[(part1, part2)]
    
    parts = [domain.MappedComponent(part_no=f'part{i}', composition=np.array([]), mass=1.0) for i in range(1,5)]
    cluster1 = domain.ComponentCluster(members=parts[0:2],
        representatives=[parts[0]])
    cluster2 = domain.ComponentCluster(members=parts[2:4],
        representatives=[parts[2]])
    clustering = domain.ComponentClustering(clusters=[cluster1, cluster2])

    all_members_mapper = ClusteringMapper(
            clustering=clustering,
            similarity_metric=similarity_function,
            compare_to=ClusteringMapper.CompareTo.ALL_MEMBERS)

    all_members_mapped = all_members_mapper('search')
    assert all_members_mapped.part_no == 'part4'

    representative_mapper = ClusteringMapper(
            clustering=clustering,
            similarity_metric=similarity_function,
            compare_to=ClusteringMapper.CompareTo.REPRESENTATIVES)

    representative_mapped = representative_mapper('search')
    assert representative_mapped.part_no == 'part1'
