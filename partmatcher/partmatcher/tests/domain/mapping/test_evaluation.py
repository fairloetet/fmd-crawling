from partmatcher import domain
from partmatcher.domain.mapping.evaluation import *

def test_absolute_error():
    assert(absolute_error(
        domain.MappedComponent(part_no='Predicted', composition=np.array([1.0, 0.0]), mass=1.0),
        domain.MappedComponent(part_no='True', composition=np.array([1.0, 0.0]), mass=1.0)
        ) == 0.0)

    assert(absolute_error(
        domain.MappedComponent(part_no='Predicted', composition=np.array([1.0, 0.0]), mass=1.0),
        domain.MappedComponent(part_no='True', composition=np.array([0.0, 1.0]), mass=1.0)
        ) == 2.0)

def test_correct_mass_percentage():
    assert(correct_mass_percentage(
        domain.MappedComponent(part_no='Predicted', composition=np.array([1.0, 0.0]), mass=1.0),
        domain.MappedComponent(part_no='True', composition=np.array([1.0, 0.0]), mass=1.0)
        ) == 1.0)

    assert(correct_mass_percentage(
        domain.MappedComponent(part_no='Predicted', composition=np.array([1.0, 0.0]), mass=1.0),
        domain.MappedComponent(part_no='True', composition=np.array([0.0, 1.0]), mass=1.0)
        ) == 0.0)