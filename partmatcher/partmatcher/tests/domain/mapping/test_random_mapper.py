import numpy as np

from partmatcher.domain import Substance
from partmatcher.domain.mapping import RandomMapper

# Some substances for testing
gold = Substance('7440-57-5')
copper = Substance('7440-50-8')
tantalum = Substance('7440-25-7')

def test_random_mapper():
    substances = [
        gold,
        copper,
        tantalum,
    ]

    mapper = RandomMapper(substances)
    mapped1 = mapper('MyComponent1')
    assert(mapped1.part_no == 'MyComponent1')
    assert(np.sum(mapped1.composition) == 1.0)
    assert(mapped1.mass > 0.0)
