from pathlib import Path

import pandas as pd

from .. import domain

def load_clustering(clustering_path: Path):
    clustering_df = pd.read_csv(clustering_path)
    return read_clustering_from_df(clustering_df)

def read_clustering_from_df(clustering_df: pd.DataFrame) -> domain.ComponentClustering:
    clusters = []
    for cluster_id, cluster_df in clustering_df.groupby('cluster'):
        components = []
        for _, row in cluster_df.iterrows():
            component = domain.MappedComponent(
                part_no=row['part_no'],
                composition=row.drop(['name', 'part_no', 'cluster']).to_numpy(),
                mass=1.0
            )
            components.append(component)
        cluster = domain.ComponentCluster(cluster_id=cluster_id, representatives=[], members=components)
        clusters.append(cluster)
    clustering = domain.ComponentClustering(clusters=clusters)
    return clustering


def load_components_from_df(components_df: pd.DataFrame) -> domain.ComponentClustering:
    components = []
    for _, row in components_df.iterrows():
        component = domain.MappedComponent(
            part_no=row['Meta', 'part_no'],
            composition=row['Substance'].to_numpy(),
            mass=1.0
        )
        components.append(component)
    return components