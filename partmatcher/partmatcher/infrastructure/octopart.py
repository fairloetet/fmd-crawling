import json
import logging
from pathlib import Path

logger = logging.getLogger(__name__)

class OctopartDumpRepository:
    """Provides access to a directory which contains downloaded Octopart data files."""

    def __init__(self, octopart_data_path: Path):
        self.data_path = octopart_data_path
        self._build_index()

    def _build_index(self):
        self.by_mpn = {}
        for file_name in self.data_path.glob('*.json'):
            try:
                with file_name.open() as f:
                    document = json.load(f)
                if 'mpn' not in document:
                    logger.warn(f'Ignoring Octopart document "{file_name}" because it does not contain an mpn')
                    continue
                mpn = document['mpn']
                if mpn in self.by_mpn:
                    logger.warn(f'Duplicate mpn "{mpn}" found in Octopart document "{file_name}"; ignoring')
                    continue
                self.by_mpn[mpn] = document
            except Exception as e:
                logger.error(f'Error loading Octopart document "{file_name}": {e}')

    def lookup(self, *, part_no: str = None):
        if part_no is not None:
            return self.by_mpn.get(part_no)
        return None

    def iterparts(self):
        for part in self.by_mpn.values():
            yield part
